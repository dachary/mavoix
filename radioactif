I. - Au 2° de l'article 3 de la loi n° 2006-739 du 28 juin 2006 de programme relative à la gestion durable des matières et déchets radioactifs, l'année : « 2015 » est remplacée par l'année : « 2018 ».

II. - L'article L. 542-10-1 du code de l'environnement est ainsi modifié :

1° Après le premier alinéa, sont insérés quatre alinéas ainsi rédigés :

« La réversibilité est la capacité, pour les générations successives, soit de poursuivre la construction puis l'exploitation des tranches successives d'un stockage, soit de réévaluer les choix définis antérieurement et de faire évoluer les solutions de gestion.

« La réversibilité est mise en oeuvre par la progressivité de la construction, l'adaptabilité de la conception et la flexibilité d'exploitation d'un stockage de déchets radioactifs en couche géologique profonde permettant d'intégrer le progrès technologique et de s'adapter aux évolutions possibles de l'inventaire des déchets consécutives notamment à une évolution de la politique énergétique. Elle inclut la possibilité de récupérer des colis de déchets déjà stockés selon des modalités et pendant une durée cohérentes avec la stratégie d'exploitation et de fermeture du stockage.

« Le caractère réversible d'un stockage en couche géologique profonde doit être assuré dans le respect de la protection des intérêts mentionnés à l'article L. 593-1. Des revues de la mise en oeuvre du principe de réversibilité dans un stockage en couche géologique profonde sont organisées au moins tous les dix ans, en cohérence avec les réexamens périodiques prévus par l'article L. 593-18.

« L'exploitation du centre débute par une phase industrielle pilote permettant de conforter le caractère réversible et la démonstration de sûreté de l'installation, notamment par un programme d'essais in situ. Tous les colis de déchets doivent rester aisément récupérables durant cette phase. La phase industrielle pilote comprend des essais de récupération de colis de déchets. » ;

2° Après le troisième alinéa, sont insérés deux alinéas ainsi rédigés :

« - les deux dernières phrases du III de l'article L. 593-6, l'alinéa 2 du III de l'article L. 593-7 et l'article L. 593-17 ne s'appliquent qu'à compter de la délivrance de l'autorisation de mise en service mentionnée à l'article L. 593-11. Celle-ci ne peut être accordée que si l'exploitant est propriétaire des terrains servant d'assiette aux installations de surface, et des tréfonds contenant les ouvrages souterrains ou s'il a obtenu l'engagement du propriétaire des terrains de respecter les obligations qui lui incombent en application de l'article L. 596-5 ;

« - pour l'application du titre IX du présent livre, les tréfonds contenant les ouvrages souterrains peuvent tenir lieu de terrain servant d'assiette pour ces ouvrages ; »

3° Le quatrième alinéa est complété par deux phrases ainsi rédigées :

« . Le délai de cinq ans mentionné à l'article L. 121-12 est porté à dix ans. Le présent alinéa ne s'applique pas aux nouvelles autorisations mentionnées à l'article L. 593-14 relatives au centre » ;

4° Après le sixième alinéa, sont insérés quatre alinéas ainsi rédigés :

« - lors de l'examen de la demande d'autorisation de création, la sûreté du centre est appréciée au regard des différentes étapes de sa gestion, y compris sa fermeture définitive. Seule une loi peut autoriser celle-ci. L'autorisation fixe la durée minimale pendant laquelle, à titre de précaution, la réversibilité du stockage doit être assurée. Cette durée ne peut être inférieure à cent ans. L'autorisation de création du centre est délivrée par décret en Conseil d'État, pris selon les modalités définies à l'article L. 593-8, sous réserve que le projet respecte les conditions fixées au présent article ;

« - l'autorisation de mise en service mentionnée à l'article L. 593-11 est limitée à la phase industrielle pilote.

« Les résultats de la phase industrielle pilote font l'objet d'un rapport de l'Agence nationale pour la gestion des déchets radioactifs, d'un avis de la commission mentionnée à l'article L. 542 3, d'un avis de l'Autorité de sûreté nucléaire et du recueil de l'avis des collectivités territoriales situées en tout ou partie dans une zone de consultation définie par décret.

« Le rapport de l'Agence nationale pour la gestion des déchets radioactifs, accompagné de l'avis de la commission nationale mentionnée à l'article L. 542-3 et de l'avis de l'Autorité de sûreté nucléaire est transmis à l'Office parlementaire d'évaluation des choix scientifiques et technologiques, qui l'évalue et rend compte de ses travaux aux commissions compétentes de l'Assemblée nationale et du Sénat. » ;

5° Le septième alinéa est ainsi rédigé :

« - le Gouvernement présente, le cas échéant, un projet de loi adaptant les conditions d'exercice de la réversibilité du stockage afin de prendre en compte les recommandations de l'Office parlementaire d'évaluation des choix scientifiques et technologiques ; »

6° Le huitième alinéa est ainsi rédigé :

« - l'Autorité de sûreté nucléaire délivre l'autorisation de mise en service complète de l'installation. Cette autorisation ne peut être délivrée à un centre de stockage en couche géologique profonde de déchets radioactifs ne garantissant pas la réversibilité de ce centre dans les conditions prévues par la loi. » ;

7° L'avant-dernier alinéa est supprimé ;

8° (nouveau) Il est ajouté un alinéa ainsi rédigé :

« Pour les ouvrages souterrains des projets de centres de stockage de déchets radioactifs en couche géologique profonde, l'autorisation de création prévue à l'article L. 542-10-1 dispense de la déclaration préalable ou du permis de construire prévus au chapitre Ier du titre II du livre IV du code de l'urbanisme. »
